"""
- CS2911 - 0NN
- Fall 2017
- Lab N
- Names:
  -
  -

A simple HTTP client
"""

# import the "socket" module -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# import the "regular expressions" module
import re


def main():
    """
    Tests the client on a variety of resources
    """

    # this resource request should result in "chunked" data transfer
    get_http_resource('http://seprof.sebern.com/', 'index.html')
    # this resource request should result in "Content-Length" data transfer
    get_http_resource('http://seprof.sebern.com/sebern1.jpg', 'sebern1.jpg')
    # get_http_resource('http://seprof.sebern.com:8080/sebern1.jpg', 'sebern2.jpg')

    # another resource to try for a little larger and more complex entity
    get_http_resource('http://seprof.sebern.com/courses/cs2910-2014-2015/sched.md', 'sched-file.md')


def get_http_resource(url, file_name):
    """
    Get an HTTP resource from a server
           Parse the URL and call function to actually make the request.

    :param url: full URL of the resource to get
    :param file_name: name of file in which to store the retrieved resource

    (do not modify this function)
    """

    # Parse the URL into its component parts using a regular expression.
    url_match = re.search('http://([^/:]*)(:\d*)?(/.*)', url)
    url_match_groups = url_match.groups() if url_match else []
    #    print 'url_match_groups=',url_match_groups
    if len(url_match_groups) == 3:
        host_name = url_match_groups[0]
        host_port = int(url_match_groups[1][1:]) if url_match_groups[1] else 80
        host_resource = url_match_groups[2]
        print('host name = {0}, port = {1}, resource = {2}'.format(host_name, host_port, host_resource))
        status_string = make_http_request(host_name.encode(), host_port, host_resource.encode(), file_name)
        print('get_http_resource: URL="{0}", status="{1}"'.format(url, status_string))
    else:
        print('get_http_resource: URL parse failed, request not sent')


def make_http_request(host, port, resource, file_name):
    """
    Get an HTTP resource from a server

    :param bytes host: the ASCII domain name or IP address of the server machine (i.e., host) to connect to
    :param int port: port number to connect to on server host
    :param bytes resource: the ASCII path/name of resource to get. This is everything in the URL after the domain name,
           including the first /.
    :param file_name: string (str) containing name of file in which to store the retrieved resource
    :return: the status code
    :rtype: int
    """

    # Written by Jeremy Schwingbeck
    # Create a new socket for transfer
    # Send request line ('GET' (URL) '1.1'CRLF
    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_socket.connect((host, port))
    print('Sending request...')
    tcp_socket.send(b'GET ' + resource + b' HTTP/1.1\x0d\x0a'
                    + b'Host: ' + host + b'\x0d\x0a\x0d\x0a')
    print('Request sent! Receiving...')
    data = tcp_socket.recv(100000)
    print('Finished receiving!')
    tcp_socket.shutdown(1)
    tcp_socket.close()
    return read_response(data, file_name)


def read_response(response, file_name):
    """

    :param file_name:
    :param response:
    :return: the status code
    """
    # Written by Matthew Rohan
    # Read in status line
    # Verify all is well
    # If not, return error code
    # Read header lines
    # Read body
    # Send body to
    chunked = 0
    content_length = 0
    message = ''
    status_code = ''
    split_response = response.split(b'\r\n')
    entries = len(split_response)
    for i in range(0, entries - 1):
        current_part = split_response.pop(0)
        if b'HTTP' in current_part:
            status_line = current_part.split(b' ')
            status_code = status_line.pop(1)
        elif b'Transfer-Encoding:' in current_part:
            if b'chunked' in current_part:
                print('Message is chunked')
                print('Response: ')
                print(response)
                chunked = 1
        elif b'Content-Length:' in current_part:
            content_length_list = current_part.split(b' ')
            content_length = content_length_list.pop(1)
            print('Content Length:')
            print(content_length)
        elif b'Content-Type:' in current_part:
            message = read_message(split_response)
            print('Message is:')
            print(message)
    print('Message Length:')
    print(len(message))
    print('\n')

    write_to_file(message, file_name)
    # Return status code
    return status_code


def read_message(message):
    compiled_message = b''
    length = len(message)
    for i in range(1, length):
        popped_part = message.pop(i)
        compiled_message += popped_part
        compiled_message += b'\r\n'
        message.insert(i, popped_part)

    return compiled_message


def read_chunked(response):
    """
    Reads a chunked body and returns a bytestring

    :author Matt Rohan
    :param response: the http response as a byte string
    :return: returns the content of the payload as a byte String
    """
    response += b'\r\n'
    print('Response is:')
    print(response)
    split_response = response.split(b'\r\n')
    print('Length of chunk list:')
    print(len(split_response))
    message = b''
    while len(split_response) != 0:
        chunk_length = split_response.pop(0)
        chunk_data = split_response.pop(0)
        if len(chunk_data) == int.from_bytes(chunk_length, 'big'):
            message += chunk_data
    return message


def write_to_file(message, file_name):
    """

    :param message:
    :param file_name:
    :return:
    """
    # Written by Jeremy Schwingbeck
    # Open a new file (file_name) to write to
    # Write message to there
    # Close file

    file = open(file_name, 'wb')
    print('Writing to file...')
    file.write(message)
    file.close()
    print('Write complete for file ' + file_name)


main()
