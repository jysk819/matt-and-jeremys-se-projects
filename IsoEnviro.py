def generate_d(e_value, z_value):
    g, x, y = euclidean_gcd(e_value, z_value)
    if g != 1:
        return None
    else:
        return x % z_value


def euclidean_gcd(a, b):
    if a == 0:
        # g gets set to b. Otherwise, g is 1
        return b, 0, 1
    else:
        g, y, x = euclidean_gcd(b % a, a)
        return g, x - (b // a) * y, y


print(generate_d(17, (222 - 1) * (223 - 1)))
