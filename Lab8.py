# Course: CS2911
# Section: 021
# Trimester: 1
# Academic Year: Sophomore
# Assignment: Lab 8
# Name: Jeremy Schwingbeck (schwingbeckjj)
# Name: Matt Rohan (rohanmj)
# File created on: 10/23/2017

"""
- CS2911 - 0NN
- Fall 2017
- Lab N
- Names:
  -Jeremy Schwingbeck
  -Matt Rohan

A simple email sending program.

Thanks to Trip Horbinski from the Fall 2015 class for providing the password-entering functionality.
"""

# GUI library for password entry
import tkinter as tk

# Socket library
import socket

# SSL/TLS library
import ssl

# base-64 encode/decode
import base64

# Python date/time and timezone modules
import datetime
import time
import Lab8
import tzlocal

# Module for reading password from console without echoing it
import getpass

# Modules for some file operations
import os
import mimetypes

# Host name for MSOE (hosted) SMTP server
SMTP_SERVER = 'smtp.office365.com'

# The default port for STARTTLS SMTP servers is 587
SMTP_PORT = 587

# SMTP domain name
SMTP_DOMAINNAME = 'msoe.edu'


def main():
    """Main test method to send an SMTP email message.

    Modify data as needed/desired to test your code,
    but keep the same interface for the smtp_send
    method.
    """
    (username, password) = login_gui()

    message_info = {}
    message_info['To'] = 'schwingbeckjj@msoe.edu'
    message_info['From'] = username
    message_info['Subject'] = 'Yet another test message'
    message_info['Date'] = 'Thu, 9 Oct 2014 23:56:09 +0000'
    message_info['Date'] = get_formatted_date()

    print("message_info =", message_info)

    message_text = 'Sounds good.\r\n\r\nWe will try and get you this before the day is out.'

    smtp_send(password, message_info, message_text)


def login_gui():
    """
    Creates a graphical user interface for secure user authorization.

    :return: (email_value, password_value)
        email_value -- The email address as a string.
        password_value -- The password as a string.

    :author: Tripp Horbinski
    """
    gui = tk.Tk()
    gui.title("MSOE Email Client")
    center_gui_on_screen(gui, 370, 120)

    tk.Label(gui, text="Please enter your MSOE credentials below:") \
        .grid(row=0, columnspan=2)
    tk.Label(gui, text="Email Address: ").grid(row=1)
    tk.Label(gui, text="Password:         ").grid(row=2)

    email = tk.StringVar()
    email_input = tk.Entry(gui, textvariable=email)
    email_input.grid(row=1, column=1)

    password = tk.StringVar()
    password_input = tk.Entry(gui, textvariable=password, show='*')
    password_input.grid(row=2, column=1)

    auth_button = tk.Button(gui, text="Authenticate", width=25, command=gui.destroy)
    auth_button.grid(row=3, column=1)

    gui.mainloop()

    email_value = email.get()
    password_value = password.get()

    return email_value, password_value


def center_gui_on_screen(gui, gui_width, gui_height):
    """Centers the graphical user interface on the screen.

    :param gui: The graphical user interface to be centered.
    :param gui_width: The width of the graphical user interface.
    :param gui_height: The height of the graphical user interface.
    :return: The graphical user interface coordinates for the center of the screen.
    :author: Tripp Horbinski
    """
    screen_width = gui.winfo_screenwidth()
    screen_height = gui.winfo_screenheight()
    x_coord = (screen_width / 2) - (gui_width / 2)
    y_coord = (screen_height / 2) - (gui_height / 2)

    return gui.geometry('%dx%d+%d+%d' % (gui_width, gui_height, x_coord, y_coord))


# *** Do not modify code above this line ***


def smtp_send(password, message_info, message_text):
    """Send a message via SMTP.

    :param password: String containing user password.
    :param message_info: Dictionary with string values for the following keys:
                'To': Recipient address (only one recipient required)
                'From': Sender address
                'Date': Date string for current date/time in SMTP format
                'Subject': Email subject
            Other keys can be added to support other email headers, etc.
    """

    smtp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    smtp_socket.connect((SMTP_SERVER, SMTP_PORT))
    response = read_response(smtp_socket)
    print('\nConnection Message:\n' + (response.decode('ASCII')))

    if response[:3] == b'220':
        smtp_socket.send(b'EHLO ' + SMTP_DOMAINNAME.encode('ASCII') + b'\r\n')
        response = read_response(smtp_socket)
        print('\nFirst Message:\n' + response.decode('ASCII'))

        if response[:3] == b'250':
            smtp_socket.send(b'STARTTLS\r\n')
            response = read_response(smtp_socket)
            print('\nSecond Message:\n' + response.decode('ASCII'))

            if response[:3] == b'220':
                context = ssl.create_default_context()
                wrapped_socket = context.wrap_socket(smtp_socket, server_hostname=SMTP_SERVER)
                wrapped_socket.send(b'EHLO ' + SMTP_DOMAINNAME.encode('ASCII') + b'\r\n')
                response = read_response(wrapped_socket)
                print('\nThird Message:\n' + response.decode('ASCII'))

                if response[:3] == b'250':
                    wrapped_socket.send(b'AUTH LOGIN\r\n')
                    response = read_response(wrapped_socket)
                    print('\nFourth Message:\n' + response.decode('ASCII'))

                    if response[:3] == b'334':
                        wrapped_socket.send(
                            base64.b64encode(message_info['From'].encode('ASCII')) + b'\r\n')
                        response = read_response(wrapped_socket)
                        print('\nFifth Message:\n' + response.decode('ASCII'))

                        if response[:3] == b'334':
                            wrapped_socket.send(
                                base64.b64encode(password.encode('ASCII')) + b'\r\n')
                            response = read_response(wrapped_socket)
                            print('\nSixth Message:\n' + response.decode('ASCII'))

                            if response[:3] == b'235':
                                wrapped_socket.send(b'MAIL FROM: <' + message_info['From'].encode('ASCII') + b'>\r\n')
                                response = read_response(wrapped_socket)
                                print('\nSeventh Message:\n' + response.decode('ASCII'))

                                if response[:3] == b'250':
                                    wrapped_socket.send(
                                        b'RCPT TO: <' + message_info['To'].encode('ASCII') + b'>\r\n')
                                    response = read_response(wrapped_socket)
                                    print('\nEighth Message:\n' + response.decode('ASCII'))

                                    if response[:3] == b'250':
                                        wrapped_socket.send(
                                            b'DATA\r\n')
                                        response = read_response(wrapped_socket)
                                        print('\nNinth Message:\n' + response.decode('ASCII'))

                                        if response[:3] == b'354':
                                            wrapped_socket.send(
                                                b'From: ' + message_info['From'].encode('ASCII') + b'\r\n')
                                            wrapped_socket.send(
                                                b'To: ' + message_info['To'].encode('ASCII') + b'\r\n')
                                            wrapped_socket.send(
                                                b'Date: ' + message_info['Date'].encode('ASCII') + b'\r\n')
                                            wrapped_socket.send(
                                                b'Subject: ' + message_info['Subject'].encode('ASCII') + b'\r\n')
                                            wrapped_socket.send(
                                                message_text.encode('ASCII') + b'\r\n.\r\n')
                                            response = read_response(wrapped_socket)
                                            print('\nTenth Message:\n' + response.decode('ASCII'))

                                            if response[:3] == b'250':
                                                wrapped_socket.send(
                                                    b'QUIT\r\n')
                                                response = read_response(wrapped_socket)
                                                print('\nEleventh Message:\n' + response.decode('ASCII'))

                                                if response[:3] == b'221':
                                                    exit()
    raise Exception


def read_response(socket):
    data = b''
    while True:
        current_data = b''
        while b'\r\n' not in current_data:
            current_data += socket.recv(1)
        data += current_data
        if b'-' not in current_data[:4]:
            return data


def read_base64_response(socket):
    data = b''
    while True:
        current_data = b''
        while b'\r\n' not in current_data:
            current_data += socket.recv(1)
        data += current_data
        if b'-' not in current_data[:4]:
            return data[:4]


# ** Do not modify code below this line. ** #

# Utility functions
# You may use these functions to simplify your code.


def get_formatted_date():
    """Get the current date and time, in a format suitable for an email date header.

    The constant TIMEZONE_NAME should be one of the standard pytz timezone names.
    If you really want to see them all, call the print_all_timezones function.

    tzlocal suggested by http://stackoverflow.com/a/3168394/1048186

    See RFC 5322 for details about what the timezone should be
    https://tools.ietf.org/html/rfc5322

    :return: Formatted current date/time value, as a string.
    """
    zone = tzlocal.get_localzone()
    print("zone =", zone)
    timestamp = datetime.datetime.now(zone)
    timestring = timestamp.strftime('%a, %d %b %Y %H:%M:%S %z')  # Sun, 06 Nov 1994 08:49:37 +0000
    return timestring


def print_all_timezones():
    """ Print all pytz timezone strings. """
    for tz in Lab8.all_timezones:
        print(tz)


# You probably won't need the following methods, unless you decide to
# try to handle email attachments or send multi-part messages.
# These advanced capabilities are not required for the lab assignment.


def get_mime_type(file_path):
    """Try to guess the MIME type of a file (resource), given its path (primarily its file extension)

    :param file_path: String containing path to (resource) file, such as './abc.jpg'
    :return: If successful in guessing the MIME type, a string representing the content
             type, such as 'image/jpeg'
             Otherwise, None
    :rtype: int or None
    """

    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


def get_file_size(file_path):
    """Try to get the size of a file (resource) in bytes, given its path

    :param file_path: String containing path to (resource) file, such as './abc.html'

    :return: If file_path designates a normal file, an integer value representing the the file size in bytes
             Otherwise (no such file, or path is not a file), None
    :rtype: int or None
    """

    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()
