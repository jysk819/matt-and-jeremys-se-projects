"""
- CS2911 - 021
- Fall 2017
- Lab 7
- Names: Matt Rohan & Jeremy Schwingbeck

A simple HTTP server
"""

import socket
import re
import threading
import os
import mimetypes
import datetime


def main():
    """ Start the server """
    http_server_setup(8080)


def http_server_setup(port):
    """
    Start the HTTP server
    - Open the listening socket
    - Accept connections and spawn processes to handle requests

    :param port: listening port number
    """

    num_connections = 10
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_address = ('', port)
    server_socket.bind(listen_address)
    server_socket.listen(num_connections)
    try:
        while True:
            request_socket, request_address = server_socket.accept()
            print('connection from {0} {1}'.format(request_address[0], request_address[1]))
            # Create a new thread, and set up the handle_request method and its argument (in a tuple)
            request_handler = threading.Thread(target=handle_request, args=(request_socket,))
            # Start the request handler thread.
            request_handler.start()
            # Just for information, display the running threads (including this main one)
            print('threads: ', threading.enumerate())
    # Set up so a Ctrl-C should terminate the server; this may have some problems on Windows
    except KeyboardInterrupt:
        print("HTTP server exiting . . .")
        print('threads: ', threading.enumerate())
        server_socket.close()


def handle_request(request_socket):
    """
    Handle a single HTTP request, running on a newly started thread.

    Closes request socket after sending response.

    Should include a response header indicating NO persistent connection

    :param request_socket: socket representing TCP connection from the HTTP client_socket
    :return: None
    :author: Jeremy Schwingbeck and Matthew Rohan
    """

    data = b''
    while b'\r\n\r\n' not in data:
        data += request_socket.recv(1)
    print('Finished receiving!')
    important_info = read_request(data)
    if important_info[0] != b'GET':
        print("Unhandled method found. Exiting...")
    else:
        if important_info[1] == b'/':
            important_info[1] = b'/index.html'
        file_path = b'.' + important_info[1]
        file = file_to_send(important_info[1])
        headers = generate_headers(get_file_size(file_path.decode('ASCII')),
                                   get_mime_type(file_path.decode('ASCII')))
        response = compile_data(headers, file)
        print(response)
        request_socket.send(response)
    request_socket.shutdown(1)
    request_socket.close()
    print('Headers:')
    print(important_info[2])


def compile_data(headers, body):
    """
    Compiles the headers and the body of the response

    :param headers: a dictionary of headers, formatted as {header : value}, in bytes
    :param body: the body of the message, in bytes
    :return: the compiled response
    :author: Jeremy Schwingbeck
    """
    response = b'HTTP/1.1 200 OK\r\n'
    for header_name in headers:
        response += header_name + headers[header_name]
    response += b'\r\n'
    response += body
    return response


def read_request(data):
    """
    Reads in the first line of the request, then calls a helper to read the headers

    :param data: the request received
    :return: a list of important information, formatted as: index 0 = Method, index 1 = File path, index 2 = headers
    :author: Jeremy Schwingbeck
    """
    returned_info = []
    request = data.split(b'\r\n')
    # Get the method
    returned_info.append(request[0].split(b' ')[0])
    # Get the URL
    returned_info.append(request[0].split(b' ')[1])
    request.pop(0)
    returned_info.append(read_headers(request))
    return returned_info


def read_headers(request):
    """
    Reads the heders in the request

    :param request: the request, minus the request line
    :return: a dictionary of headers
    :author: Jeremy Schwingbeck
    """
    header_lines = {}
    for header_line in request:
        if len(header_line) is not 0:
            line = header_line.split(b' ')
            header_lines[line[0]] = line[1]
    return header_lines


def file_to_send(url):
    """
    Finds the file to send

    :param url: the file path received, minus a "."
    :return: the data in the file
    :author: Matthew Rohan
    """
    data = b''
    if url is not None:
        if (url == b'/') or (url == b'/index.html'):
            f = open('index.html', 'rb')
            data = f.read()
            f.close()
        elif url == b'/sebern1.jpg':
            f = open('sebern1.jpg', 'rb')
            data = f.read()
            f.close()
        elif url == b'/style.css':
            f = open('style.css', 'rb')
            data = f.read()
            f.close()
        elif url == b'/e-sebern2.gif':
            f = open('e-sebern2.gif', 'rb')
            data = f.read()
            f.close()
        return data


def generate_headers(file_size, mime_type):
    """
    Generates headers for the response

    :param file_size: the size of the file being sent
    :param mime_type: the MIME type, from get_mime_type
    :return: a dictionary of headers
    :author: Matthew Rohan
    """
    headers = {}
    headers[b'Date: '] = b'JMPythonServer\r\n'
    headers[b'Connection: '] = b'close\r\n'
    file_size = str(file_size)
    headers[b'Content-Length: '] = file_size.encode('ASCII') + b'\r\n'
    headers[b'Content-Type: '] = b'' + mime_type.encode('ASCII') + b'\r\n'
    return headers


# ** Do not modify code below this line.  You should add additional helper methods above this line.

# Utility functions
# You may use these functions to simplify your code.


def get_mime_type(file_path):
    """
    Try to guess the MIME type of a file (resource), given its path (primarily its file extension)

    :param file_path: string containing path to (resource) file, such as './abc.html'
    :return: If successful in guessing the MIME type, a string representing the content type, such as 'text/html'
             Otherwise, None
    :rtype: int or None
    """

    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


def get_file_size(file_path):
    """
    Try to get the size of a file (resource) as number of bytes, given its path

    :param file_path: string containing path to (resource) file, such as './abc.html'
    :return: If file_path designates a normal file, an integer value representing the the file size in bytes
             Otherwise (no such file, or path is not a file), None
    :rtype: int or None
    """

    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()

# Replace this line with your comments on the lab
